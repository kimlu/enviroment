<?php
declare( strict_types=1 );

namespace tests\kimlu\environment;

use PHPUnit\Framework\TestCase;
use kimlu\environment\Env;

class EnvTest extends TestCase
{
    
    /**
     *
     * @var string
     */
    private $jsonFilePath = null;
    
    public function setUp(): void
    {
        $this->jsonFilePath = __DIR__.DIRECTORY_SEPARATOR.'env.json';
    }
    
    public function tearDown(): void
    {
        unlink( $this->jsonFilePath );
    }
    
    public function testStart()
    {
        Env::start( __DIR__, 'env.json' );
        $this->assertEquals( __DIR__.DIRECTORY_SEPARATOR, Env::config()->pathEnvironment() );
    }
}
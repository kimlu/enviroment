<?php
declare( strict_types=1 );

namespace kimlu\entities;

use stdClass;
use kimlu\entities\models\FileLogErrorModel;
use kimlu\utils\Path;
use kimlu\handler\errors\ErrorContext;
use kimlu\handler\errors\HTTPServerErrorContext;

/**
 *
 * @author Máximo
 *        
 */
class EnvironmentConfiguration extends JsonEntity
{
    
    /**
     * 
     * @param string $filepath
     * @param bool $lazyLoad
     */
    public function __construct( string $filepath, bool $lazyLoad = false )
    {
        parent::__construct( $filepath, $lazyLoad );
        
    }
    
    /**
     * 
     * @return FileLogErrorModel|null
     */
    public function handlerErrors(): ?ErrorContext
    {
        $handlerError = null;
        if ( 
            isset( $this->data->handler ) 
                && isset( $this->data->handler->errors ) 
                    && ( $this->data->handler->errors->classname ) )
        {
            if ( $this->data->handler->errors->classname == FileLogErrorModel::class )
            {
                $handlerError = new FileLogErrorModel( $this->data->manager->errors );
            }
            elseif ( $this->data->handler->errors->classname == HTTPServerErrorContext::class )
            {
                $handlerError = new HTTPServerErrorContext( $this->data->manager->errors );
            }
        }
        return $handlerError;
    }
    
    /**
     * 
     * @param string $path
     * @return string
     */
    public function pathEnvironment( string $path = NULL ) : string
    {
        if ( !isset( $this->data()->paths ) )
        {
            $this->data()->paths = new stdClass();
        }
        if ( !isset( $this->data()->paths->env ) )
        {
            $this->data()->paths->env = '';
        }
        if ( isset( $path ) )
        {
            $this->data()->paths->env = Path::forceDirBase( $path );
            $this->sincronize();
        }
        return  $this->data()->paths->env;
    }
}
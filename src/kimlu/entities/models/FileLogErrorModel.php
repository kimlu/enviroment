<?php
declare( strict_types=1 );

namespace kimlu\entities\models;

use stdClass;
use kimlu\utils\Path;


/**
 *
 * @author Máximo
 *        
 */
class FileLogErrorModel extends JsonModel
{
    
    /**
     * 
     * @param string $fileLogPath
     * @return string
     */
    public function fileLogPath( string $fileLogPath = null ) : string
    {
        if ( !isset( $this->data()->args ) )
        {
            $this->data()->args = new stdClass();
        }
        if ( !isset( $this->data()->args->fileLogPath ) )
        {
            $this->data()->args->fileLogPath = 'env.log';
        }
        if ( isset( $fileLogPath ) )
        {
            $this->data()->args->fileLogPath = Path::safe( $fileLogPath ); 
        }
        return $this->data()->args->fileLogPath;
    }
    
    /**
     *
     * @param stdClass $args
     * @return stdClass
     */
    public function args( stdClass $args = null ) : stdClass
    {
        if ( isset( $args ) )
        {
            $this->data()->args = $args;
        }
        if ( isset( $this->data()->args ) )
        {
            return $this->data()->args;
        }
        return new stdClass();
    }
    
    public function argsAsArray(): array
    {
        return json_decode( json_encode( $this->args() ), true );
    }
    
}
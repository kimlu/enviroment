<?php
declare( strict_types=1 );

namespace kimlu\environment;

use ReflectionMethod;
use stdClass;
use kimlu\entities\EnvironmentConfiguration;
use kimlu\utils\Path;
use kimlu\utils\TXT;

/**
 *
 * @author Máximo
 *        
 */
abstract class ExecutionEnvironment
{

    /**
     * 
     * @var string
     */
    const EXECUTE_MODE_CLI = 'EXECUTE_MODE_CLI';
    
    /**
     *
     * @var string
     */
    const EXECUTE_MODE_CGI = 'EXECUTE_MODE_CGI';
    
    /**
     * 
     * @var EnvironmentConfiguration
     */
    static protected $config = null;

    /**
     * 
     * @var stdClass
     */
    static protected $handlerErrors = null;
    
    /**
     * 
     * @param string $filePathConfig
     */
    static public function start( string $pathEnvironment, string $filePathConfig )
    {
        $pathEnvironment = Path::forceDirBase( $pathEnvironment );
        set_include_path( get_include_path().PATH_SEPARATOR.$pathEnvironment );
        self::$config = new EnvironmentConfiguration( $pathEnvironment.$filePathConfig );
        self::config()->pathEnvironment( $pathEnvironment );
        self::startErrorsHandler();
    }
    
    /**
     * 
     */
    static private function startErrorsHandler()
    {
        if ( self::config()->handlerErrors() !== NULL )
        {
            $handlerErrors = self::config()->handlerErrors();
            $classname = $handlerErrors->classname();
            $method = new ReflectionMethod( "{$classname}::start" );
            $method->invoke( NULL , $handlerErrors->argsAsArray() );
        }               
    }
    
    /**
     * 
     * @return EnvironmentConfiguration
     */
    static public function config() : EnvironmentConfiguration
    {
        return self::$config;
    }
    
    /**
     * 
     * @return bool
     */
    static public function isModeCGI() : bool
    {
        return !self::isModeCLI();
    }
    
    /**
     * 
     * @return bool
     */
    static public function isModeCLI() : bool
    {
        return defined( 'STDIN' ) 
                        || ( empty( $_SERVER[ 'REMOTE_ADDR' ] ) 
                                    && !isset( $_SERVER[ 'HTTP_USER_AGENT' ] ) 
                                        && count( $_SERVER[ 'argv' ] ) > 0 );
    }
    
    /**
     * 
     * @param string $path
     * @return string
     */
    static public function fullPathSafe( string $relativePath ) : string
    {
        $relativePath = Path::safe( $relativePath );
        if ( TXT::create( $relativePath )->startWith( DIRECTORY_SEPARATOR ) )
        {
            $relativePath = substr( $relativePath, 1 );
        }
        return Path::safe( self::config()->pathEnvironment().$relativePath );
    }
}
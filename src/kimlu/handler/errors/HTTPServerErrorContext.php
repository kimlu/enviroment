<?php
declare( strict_types=1 );

namespace kimlu\handler\errors;

use Throwable;
use kimlu\environment\Env;

/**
 *
 * @author Máximo
 *        
 */
class HTTPServerErrorContext extends FileLogErrorContext
{

    /**
     * 
     * @param Throwable $exception
     */
    static public function exception_handler ( Throwable $exception )
    {
        error_log( $exception );
        if ( Env::isModeCGI() )
        {
            $respuesta = $exception->getMessage();
            header( 'HTTP/1.1 500 Internal Server Error' );
            header('Content-Length: '. strlen( $respuesta ) );
            ob_start();
            print $respuesta;
            ob_end_flush();
        }
        exit();
    }

}


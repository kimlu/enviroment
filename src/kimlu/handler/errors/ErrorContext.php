<?php
declare( strict_types=1 );

namespace kimlu\handler\errors;

use Throwable;

/**
 *
 * @author Máximo
 *        
 */
interface ErrorContext
{
    
    /**
     * 
     * @param array $parameters
     */
    static public function start( array $parameters = null );
    
    /**
     * 
     */
    static public function shutdown_handler();
    
    /**
     * 
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param int $errline
     * @param array $errcontext
     * @return bool
     */
    static public function error_handler( int $errno , string $errstr, string $errfile, int $errline, array $errcontext ) : bool;

    /**
     * 
     * @param Throwable $exception
     */
    static public function exception_handler( Throwable $exception ) ;
    
}
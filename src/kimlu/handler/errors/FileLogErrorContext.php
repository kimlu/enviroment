<?php
declare( strict_types=1 );

namespace kimlu\handler\errors;

use ErrorException;
use Throwable;
use kimlu\environment\ExecutionEnvironment;

/**
 *
 * @author Máximo
 *        
 */
abstract class FileLogErrorContext implements ErrorContext
{
    /**
     * 
     * @param array $parameters
     */
    static public function start ( array $parameters = null )
    {
        $fileLogPath = 'env.log';
        if ( isset( $parameters ) && isset( $parameters[ 'fileLogPath' ] ) )
        {
            $fileLogPath = ExecutionEnvironment::fullPathSafe( $parameters[ 'fileLogPath' ] );
        }
        ini_set( 'log_errors', TRUE  );
        ini_set( 'error_log', $fileLogPath );
        ini_set( 'display_errors', 'stderr' );
        error_reporting( E_ALL );
        
        $classname = static::class;
        set_error_handler( "{$classname}::error_handler" );
        set_exception_handler( "{$classname}::exception_handler" );
        register_shutdown_function( "{$classname}::shutdown_handler" );
    }

    /**
     * 
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param int $errline
     * @param array $errcontext
     * @return bool
     */
    static public function error_handler ( int $errno , string $errstr, string $errfile, int $errline, array $errcontext = null ) : bool
    {
        error_log( new ErrorException( $errstr, 0, $errno, $errfile, $errline ) );
        return FALSE;
    }

    /**
     * 
     * @param Throwable $exception
     */
    static public function exception_handler ( Throwable $exception )
    {
        error_log( $exception );
        exit();
    }

    /**
     * 
     */
    static public function shutdown_handler ()
    {
        $error = error_get_last();
        if( isset( $error ) )
        {
            error_log( new ErrorException( $error[ 'message' ], 0, $error[ 'type' ], $error[ 'file' ], $error[ 'line' ] ) );
        }
        exit();
    }
}
